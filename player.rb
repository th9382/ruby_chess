class HumanPlayer
  attr_reader :color

  COLUMN_MAP = {'a' => 0,
                'b' => 1,
                'c' => 2,
                'd' => 3,
                'e' => 4,
                'f' => 5,
                'g' => 6,
                'h' => 7  }

  ROW_MAP = (1..8).to_a.reverse

  def initialize(color)
    @color = color
  end

  def play_turn
    msg = "Color: #{color}. Please input start coordinates (ex. a1): "
    row_start, col_start = get_prompt(msg)

    msg = "Color: #{color}. Please input end coordinates (ex. a1): "
    row_end, col_end = get_prompt(msg)

    [[row_start, col_start], [row_end, col_end]]
  end

  def get_prompt(msg)
    puts msg
    input = gets.chomp.split('')

    row = ROW_MAP[input[1].to_i]
    col = COLUMN_MAP[input[0]]

    [row, col]
  end
end
