# -*- coding: utf-8 -*-
require_relative './stepping_pieces.rb'

class King < SteppingPieces
  def initialize(color, pos, board)
    super
  end

  def symbol
    { black: '♚', white: '♔' }
  end

  def move_dirs
    [[1, 1], [-1, 1], [1, -1], [-1, -1],
     [0, 1], [-1, 0], [0, -1], [1, 0]]
  end
end
