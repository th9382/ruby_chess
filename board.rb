require_relative './pieces.rb'
require 'colorize'
require 'byebug'

class InvalidMovesError < StandardError
end

class Board
  attr_reader :grid

  def initialize(place_pieces = true)
    @grid = Array.new(8) { Array.new(8) }
    build_board if place_pieces
  end

  def [](pos)
    x, y = pos
    @grid[x][y]
  end

  def []=(pos, piece)
    x, y = pos
    @grid[x][y] = piece
  end

  def empty?(pos)
    self[pos].nil?
  end

  def add_piece(pos, piece)
    self[pos] = piece
  end

  def in_check?(color)
    king_pos = find_king(color).pos
    other_color = (color == :white ? :black : :white)

    pieces(other_color).each do |piece|
      return true if piece.moves.include?(king.pos)
    end

    false
  end

  def check_mate?(color)
    return false unless in_check?(color)

    pieces(color).all? do |piece|
      piece.valid_moves.empty?
    end
  end

  def find_king(color)
    pieces(color).select { |piece| piece.is_a?(King) }.first
  end

  def inspect
    "this board"
  end

  def pieces(color)
    @grid.flatten.compact.select do |piece|
      piece.color == color
    end
  end

  def dup
    new_board = Board.new(false)
    all_pieces.each do |piece|
      new_piece = piece.class.new(piece.color, piece.pos, new_board)
      new_board[new_piece.pos] = new_piece
    end

    new_board
  end

  def move(color, start_pos, end_pos)
    raise InvalidMovesError.new("From position is empty") if empty?(start_pos)

    current_piece = self[start_pos]
    if current_piece.color != color
      raise InvalidMovesError.new("Wrong color")
    end
    raise InvalidMovesError.new("Invalid Move") unless current_piece.in_bound?(end_pos)
    raise InvalidMovesError.new("Can't move there") if current_piece.blocked_by_self?(end_pos)

    possible_moves = current_piece.valid_moves

    raise InvalidMovesError.new("Invalid Move") unless possible_moves.include?(end_pos)

    move!(start_pos, end_pos)
  end

  def move!(start_pos, end_pos)
    current_piece = self[start_pos]

    if possible_moves.include?(end_pos)
      self[end_pos] = current_piece
      self[start_pos] = nil
      current_piece.pos = end_pos
    end

    nil
  end

  def build_pawn_row(color)
    row_idx = (color == :white) ? 6 : 1

    8.times do |col_idx|
      Pawn.new(color, [row_idx, col_idx], self)
    end
  end

  def build_board
    build_pawn_row(:black)
    build_pawn_row(:white)
    build_back_row(:black)
    build_back_row(:white)
  end

  def build_back_row(color)
    # black on top, white on bottom
    x_color = (color == :white ? 7 : 0)
    formation = [Rook, Knight, Bishop, Queen, King, Bishop, Knight, Rook]

    formation.each_with_index do |piece, idx|
      piece.new(color, [x_color, idx], self)
    end

  end

  def render
    inverse_row = (1..8).to_a.reverse

    new_str = "\n"

    @grid.each_with_index do |row, row_idx|
      new_str << inverse_row[row_idx].to_s + " "
      row.each_with_index do |piece, col_idx|

        if (row_idx.even? && col_idx.even?) || (row_idx.odd? && col_idx.odd?)
          if piece.nil?
            new_str << (" " + "  ") .colorize(:background => :light_cyan)
          else
            new_str << (" " + piece.render.to_s + " ") .colorize(:background => :light_cyan)
          end
        else
          if piece.nil?
            new_str << (" " + "  ") .colorize(:background => :cyan)
          else
            new_str << (" " + piece.render.to_s + " ") .colorize(:background => :cyan)
          end
        end

      end
      new_str << "\n"

    end

    new_str << "   " << ('a'..'h').to_a.join("  ") << "\n"

    new_str
  end

  def display
    puts render
  end
end
