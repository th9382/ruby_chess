# -*- coding: utf-8 -*-
require_relative './sliding_pieces.rb'

class Rook < SlidingPieces
  def initialize(color, pos, board)
    super
  end

  def symbol
    { black: '♜', white: '♖' }
  end

  def move_dirs
    [[0, 1], [-1, 0], [0, -1], [1, 0]]
  end
end
