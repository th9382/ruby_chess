require_relative './board.rb'
require_relative './player.rb'

 class Game
   def initialize
     @board = Board.new
     @player1 = HumanPlayer.new(:white)
     @player2 = HumanPlayer.new(:black)
     @current_player = @player1
   end

   def play
     until over?
       @board.display
       
       start_pos, end_pos = @current_player.play_turn
       @board.move(@current_player.color, start_pos, end_pos)

       @current_player = (@current_player.color == :white ? @player2 : @player1 )
     end

     if @board.check_mate?(:white)
       puts 'White lost!'
     else
       puts 'Black lost!'
     end

   rescue InvalidMovesError => error
     p error
     retry

   end

   def over?
     @board.check_mate?(:white) || @board.check_mate?(:black)
   end
 end
